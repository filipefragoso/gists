//
//  GistsTests.swift
//  GistsTests
//
//  Created by Filipe Augusto de Souza Fragoso on 5/17/16.
//  Copyright © 2016 fragoso. All rights reserved.
//

import XCTest
@testable import Gists

class GistsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testLoadImage() {
        let service = WebService.sharedInstance
        
        XCTAssertNil(service.loadImage(""))
        XCTAssertNil(service.loadImage("blabla"))
        XCTAssertNotNil(service.loadImage("https://avatars.githubusercontent.com/u/2161075?v=3"))
    }
    
    func testLoadGists() {
        let expectation = expectationWithDescription("Gists")
        
        let service = WebService.sharedInstance
        service.getGists{ (gistList : [Gist]?) in
            XCTAssertNotNil(gistList, "Expected non-nil Array of Gists")
            expectation.fulfill()
        }
        
        waitForExpectationsWithTimeout(5.0, handler: nil)
    }
    
    
    func testLoadFile() {
        let expectation = expectationWithDescription("LoadFile")
        
        let service = WebService.sharedInstance
        let url = "https://gist.githubusercontent.com/B1anc0N1n0/33ea680feb9f4f8cbb57e1188c413de5/raw/69db2f4f883fedc04cf4f7e0700b160a586d9336/gistfile1.txt"
        service.getFileWithUrl(url, completionHandler: { (content: String?) in
                XCTAssertNotNil(content, "Expected non-nil string Content")
                expectation.fulfill()
        })
    
        waitForExpectationsWithTimeout(5.0, handler: nil)
    }
    
    
    func testLoadFile2() {
        let expectation = expectationWithDescription("LoadFileError")
        
        let service = WebService.sharedInstance


        let url = "http://creationview.com/image/Birds4F.jpg"
        service.getFileWithUrl(url, completionHandler: { (content: String?) in
            XCTAssertNil(content, "Expected non-nil string Content")
            expectation.fulfill()
        })
        
        waitForExpectationsWithTimeout(5.0, handler: nil)
    }
    
}
