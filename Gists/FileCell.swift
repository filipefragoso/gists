//
//  FileCell.swift
//  Gists
//
//  Created by Filipe Augusto de Souza Fragoso on 5/21/16.
//  Copyright © 2016 fragoso. All rights reserved.
//

import UIKit

class FileCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.blackColor().CGColor
    }
}
