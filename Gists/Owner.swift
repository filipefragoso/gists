//
//  Owner.swift
//  Gists
//
//  Created by Filipe Augusto de Souza Fragoso on 5/18/16.
//  Copyright © 2016 fragoso. All rights reserved.
//

import Foundation
import ObjectMapper

class Owner : Mappable {
    var login: String?
    var avatar: String?
    var avatarImage : UIImage?
    let service = WebService.sharedInstance
    required init?(_ map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        login  <- map["login"]
        avatar <- map["avatar_url"]
        
        if let url = avatar {
            avatarImage = service.loadImage(url)
        }
    }
    
}
