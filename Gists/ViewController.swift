//
//  ViewController.swift
//  Gists
//
//  Created by Filipe Augusto de Souza Fragoso on 5/17/16.
//  Copyright © 2016 fragoso. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var reloadButton: UIButton!
    
    var gistsList = [Gist]()
    var filteredGistsList = [Gist]()
    var inSearchMode = false
    var service = WebService.sharedInstance
    override func viewDidLoad() {
        super.viewDidLoad()

        collection.delegate = self
        collection.dataSource = self
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.Done
        
        getGist()

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
    }
    
    func getGist() {
        activityIndicator.startAnimating()
        
        service.getGists { (gistList : [Gist]?) in
            if gistList != nil {
                self.gistsList = gistList!
                self.collection.hidden = false
                self.collection.reloadData()
            } else {
                let myMessage = "Service Error. Please check your internet connection"
                let myAlert = UIAlertController(title: myMessage, message: nil, preferredStyle: UIAlertControllerStyle.Alert)
                myAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(myAlert, animated: true, completion: nil)
                self.collection.hidden = true
                self.reloadButton.hidden = false
            }
            
            self.activityIndicator.stopAnimating()
            self.activityIndicator.hidesWhenStopped = true
            
        }
    }

    @IBAction func reloadButtonTouched(sender: AnyObject) {
        self.reloadButton.hidden = true
        getGist()
    }
// MARK: - Collection methods
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCellWithReuseIdentifier("GistCell", forIndexPath: indexPath) as? GistCell {
            
            let gist: Gist!
            
            if inSearchMode{
                gist = filteredGistsList[indexPath.row]
            } else {
              gist = gistsList[indexPath.row]
            }
            
            cell.configureCell(gist)
            
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func  collectionView(collectionView: UICollectionView, didDeselectItemAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let gist: Gist!
        if inSearchMode{
            gist = filteredGistsList[indexPath.row]
        } else {
            gist = gistsList[indexPath.row]
        }
        
        performSegueWithIdentifier("detailsSegue", sender: gist)
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if inSearchMode{
           return filteredGistsList.count
        }
    
        return gistsList.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(100, 163)
    }

// MARK: - SearchBar methods
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == "" {
            inSearchMode = false
            view.endEditing(true)
            collection.reloadData()
        } else {
            inSearchMode = true
            let lower = searchBar.text!.lowercaseString
            filteredGistsList = gistsList.filter({$0.type.rangeOfString(lower) != nil })
            collection.reloadData()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "detailsSegue" {
            if let detailsVC = segue.destinationViewController as? DetailsViewController {
                if let poke = sender as? Gist {
                    detailsVC.gist = poke
                }
            }
        }
    }

}

