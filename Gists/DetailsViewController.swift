//
//  DetailsViewController.swift
//  Gists
//
//  Created by Filipe Augusto de Souza Fragoso on 5/21/16.
//  Copyright © 2016 fragoso. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    var gist: Gist!
    var filesList: [File] = []
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var desc: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 80
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.setNeedsLayout()
        tableView.layoutIfNeeded()

        
        gist.loadFiles(){ (result: [File]) in
            self.filesList = result
            self.tableView.reloadData()
        }
        
        
        if let owner = self.gist.owner {
            nameLabel.text = owner.login
            photo.image = owner.avatarImage
        } else {
            nameLabel.text = "--"
            photo.image = UIImage(named: "emptyProfilee")
        }
    }
    
    // MARK: - Table View
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filesList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("fileCell", forIndexPath: indexPath) as! FileCell
        //let cell = UITableViewCell()
        
        cell.nameLabel.text = self.filesList[indexPath.row].name

        cell.contentLabel.text = self.filesList[indexPath.row].content
        if(countLabelLines(cell.contentLabel) > 300){
            cell.contentLabel.text = "FILE IS TO LARGE TO BE DISPLAYED"
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    
    }
    
    func countLabelLines(label: UILabel) -> Int {
        // Call self.layoutIfNeeded() if your view is uses auto layout
        let myText = label.text! as NSString
        
        let attributes = [NSFontAttributeName : label.font]
        let labelSize = myText.boundingRectWithSize(CGSizeMake(label.bounds.width, CGFloat.max), options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attributes, context: nil)
        
        return Int(ceil(CGFloat(labelSize.height) / label.font.lineHeight))
    }
}
