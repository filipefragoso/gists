//
//  GistCell.swift
//  Gists
//
//  Created by Filipe Augusto de Souza Fragoso on 5/18/16.
//  Copyright © 2016 fragoso. All rights reserved.
//

import UIKit

class GistCell: UICollectionViewCell {
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var languageLabel : UILabel!
    @IBOutlet weak var typeLabel : UILabel!
    
    var gist: Gist!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.blackColor().CGColor
    }
    
    func configureCell(gist: Gist) {
        self.gist = gist
        
        let language = self.gist.language
        if  language != "" {
            languageLabel.text = self.gist.language
        } else {
            "--"
        }
        
        let type = self.gist.type
        if type != "" {
            typeLabel.text = self.gist.type
        } else {
            "--"
        }

        if let owner = self.gist.owner {
            nameLabel.text = owner.login
            photo.image = owner.avatarImage
        } else {
            nameLabel.text = "--"
            photo.image = UIImage(named: "emptyProfilee")
        }
    }
}
