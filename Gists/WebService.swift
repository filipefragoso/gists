//
//  WebService.swift
//  Gists
//
//  Created by Filipe Augusto de Souza Fragoso on 5/21/16.
//  Copyright © 2016 fragoso. All rights reserved.
//

import Foundation
import Alamofire

class WebService : NSObject{
    static let sharedInstance = WebService()
    
    let URL = "https://api.github.com/gists/public?page=0"

    func loadImage(url: String) -> UIImage?{
        if let url = NSURL(string: url){
            if let data = NSData(contentsOfURL: url){
                return UIImage(data: data)!
            }
        }
        
        return nil
    }
    
    func getGists(completionHandler: (([Gist]?)->())?) {
        Alamofire.request(.GET, URL).responseArray { (response: Response<[Gist], NSError>) in
            
            if response.result.error != nil {
                    completionHandler?(nil)
            } else {
                let gistArray = response.result.value
                completionHandler?(gistArray)
            }
            
        }
    }
    
    func getFileWithUrl(url: String, completionHandler: ((String?)->())?) {
        Alamofire.request(.GET, url)
        .validate()
        .responseString { response in
            if let error = response.result.error {
                print("ERROR \(error.description)")
                completionHandler?(nil)
            } else {
                let fileContent = response.result.value
                completionHandler?(fileContent)
            }
        }
    }

}
