//
//  File.swift
//  Gists
//
//  Created by Filipe Augusto de Souza Fragoso on 5/20/16.
//  Copyright © 2016 fragoso. All rights reserved.
//

import Foundation
import ObjectMapper

class File : Mappable {
    var type: String?
    var language: String?
    var raw_url: String?
    var name:String?
    var content: String!
    
    required init?(_ map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        language  <- map["language"]
        type      <- map["type"]
        raw_url   <- map["raw_url"]
        name  <- map["filename"]
    }

}