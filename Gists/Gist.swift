//
//  Gist.swift
//  Gists
//
//  Created by Filipe Augusto de Souza Fragoso on 5/18/16.
//  Copyright © 2016 fragoso. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire

class Gist : Mappable {
    var filesDic: [String : File]?
    var owner: Owner?
    var count  = 0
    let service = WebService.sharedInstance
    required init?(_ map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        filesDic     <- map["files"]
        owner     <- map["owner"]
    }
    
    var language: String{
        if let files = self.filesDic {
            for (_,file) in files {
                if let language = file.language {
                    return language
                }
                break
            }
        }
        return ""
    }
    
    var type: String{
        if let files = self.filesDic {
            for (_,file) in files {
                if let type = file.type {
                    return type
                }
                break
            }
        }
        return ""
    }
    
    func loadFiles(completion: (result: [File]) -> Void) {
        count = 0
        var raw_url = ""
        if let files = self.filesDic {
            for (_,file) in files {
                if let url = file.raw_url {
                    raw_url = url
                    service.getFileWithUrl(raw_url, completionHandler: { (fileContent: String?) in
                        if fileContent != nil {
                            file.content = fileContent
                        }
                        
                        self.count += 1
                        if(self.count == self.filesDic?.count){
                            print("FIM DO LOOP")
                            completion(result: self.getFilesList)
                        }
                    })
                }
            }
        }
    }
    
    var getFilesList:[File] {
        var filesList = [File]()
        if let files = self.filesDic {
            for (_,file) in files {
                filesList.append(file)
            }
        }
        return filesList
    }
}
